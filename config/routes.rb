Rails.application.routes.draw do
  

  get 'logout' => 'sessions#logout'
  get 'login' => 'sessions#login'
  post 'login_process' => 'sessions#login_process'
  get 'welcome_user' => 'sessions#welcome_user'
  get 'project_search' => 'projects#project_search'


  resources :projects
  resources :categories
  resources :users
  
  root 'sessions#login'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
