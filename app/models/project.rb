class Project < ApplicationRecord
  belongs_to :user
  belongs_to :category
  validates :project_name, presence: true, uniqueness: { scope: :category_id }
  include PgSearch::Model
      pg_search_scope :search_by_project_name, :against => :project_name, 
      	using: {
      		tsearch: {

      			prefix: true,
      			highlight: {
        			start_sel: '<b>',
        			stop_sel: '</b>',
		      	}

      		}
      	}
end
